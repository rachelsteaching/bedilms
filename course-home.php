<? $pageTitle = "Course Home"; ?>
<? include_once( "lms-content/php/layouts/course-header.php" ); ?>


    <h1> <?= $course['course-code'] ?>: <?= $course['course-name'] ?> </h1>
    <p><?= $course['school'] ?></p>

    <div class="row">
        <div class="col-md-8">
            <a name="courses">&nbsp;</a><h2> Course Information </h2>

            <table class="table">
                <tr>
                    <th>Semester</th>
                    <td><?=$course['semester']?><br>
                        <?= date( "F j, Y", strtotime( $course['first-day-first-week'] ) ) ?> -
                        <?= date( "F j, Y", strtotime( $course['last-day-last-week'] ) ) ?>
                     </td>
                </tr>
                <tr>
                    <th>Schedule</th>
                    <td>
                        <? $first = true;
                            foreach ( $course['class-days'] as $day ) {
                                if ( !$first ) { echo( "/" ); }
                                echo( $day );
                                $first = false;
                            } ?>
                        <?=$course['time-start']?> - <?=$course['time-end']?>
                    </td>
                </tr>
                <tr>
                    <th>Location</th>
                    <td><?=$course['location']?></td>
                </tr>
                <tr>
                    <th>Instructor</th>
                    <td><?=$page['instructor']?></td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>
                        <? foreach ( $page['phones'] as $phone ) { ?>
                            <p><?=$phone?></p>
                        <? } ?>
                    </td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>
                        <? foreach ( $page['emails'] as $email ) { ?>
                            <p><a href="mailto:<?=$email?>"><?=$email?></a></p>
                        <? } ?>
                    </td>
                </tr>
            </table>
        </div>

        <div class="col-md-4">
            <h3>Additional Resources</h3>

            <ul>
                <? foreach( $course['additional-resources'] as $resource ) { ?>
                    <li> <a href="<?=$resource['url']?>"><?=$resource['name']?></a> </li>
                <? } ?>
            </ul>
        </div>
    </div>
    
    

<? include_once( "lms-content/php/layouts/home-footer.php" ); ?>
