<? $pageTitle = "View By Week"; ?>
<? include_once( "lms-content/php/layouts/home-header.php" ); ?>
<? $courses = Storage::AllCourses(); ?>

<style type="text/css">
.due-date { font-size: 0.8em; color: #f00; }
.weekbyweek-days { font-size: 0.9em; }
</style>

<?
function ListItem( $item, $type )
{
    $iconName = strtolower( $type );

    $string = "<li>";

    if ( $item['url'] != "" )
    {
        $string .= "<a href='" . $item['url'] . "'>";
    }

    if ( $iconName != "" )
    {
        $string .= "<img src='lms-content/graphics/" . $iconName . "-icon.png' title='" . $type . "'> " . $item['name'];
    }

    if ( $item['url'] != "" )
    {
        $string .= "</a>";
    }

    if ( $item['due'] != "" )
    {
        $string .= "<div class='due-date'> Due " . $item['due'] . "</div>";
    }
    
    return $string;
}
?>

    <div class="row">
        <div class="col-md-2">
            <h3>Jump to week</h3>


            <?
                $index = 0;
                while ( isset( $courses[$index]['legacy-course'] ) )
                {
                    $index++;
                }
                $currentCourse = $courses[$index];
            ?>
            
            <? $currentWeek = Storage::GetTodaysClassWeek( $currentCourse ); ?>

            <ul class="list-of-weeks">
                <? for ( $w = 1; $w <= Storage::GetTotalClassWeeks( $currentCourse ); $w++ ) { ?>
                    <li <? if ( $currentWeek == $w ) { ?> class="this-week" <? } ?>> <a href="#week-<?=$w?>"> <span class="week">Week <?=$w?></span> <span class="date"> <?= Storage::MonthDate( Storage::GetDayOfClass( $w, "Monday", $currentCourse ) ) ?></span></a> </li>
                <? } ?>
            </ul>
        </div>
        <div class="col-md-10">
            <a name="courses">&nbsp;</a><h2> Courses by week </h2>
            <p> All courses </p>

            <table class="table week-by-week">
                <tr>
                    <th> Week </th>
                    <th> Days </th>
                    <th> Topics </th>
                    <th> In class </th>
                    <th> Homework </th>
                    <th> Notes </th>
                </tr>
                <? for ( $w = 1; $w <= Storage::GetTotalClassWeeks( $currentCourse ); $w++ ) { ?>
                    <tr class="lms-anchor-link"><td colspan="6"><a name="week-<?=$w?>"> &nbsp; </a></td></tr> <!-- Link -->
                    <tr  <? if ( $currentWeek == $w ) { ?> class="this-week" <? } ?>>
                        <!-- Week -->       <td> <?=$w?> </td> 
                        <!-- Days -->       <td class="weekbyweek-days">
                                                <? foreach ( $course['class-days'] as $day ) { ?>
                                                    <p><?= Storage::MonthDate( Storage::GetDayOfClass( $w, $day, $currentCourse ) ) ?></p>
                                                <? } ?>
                                            </td>
                                            
                        <!-- Topics -->     <td>
                                                <? foreach ( $courses as $key => $course ) { ?>
                                                    <? if ( !isset( $course['legacy-course'] ) ) { ?>
                                                        <p class='course<?=$key?>-background-light'><?= $course['course-code'] ?></p>
                                                        <ul>
                                                            <? foreach( $course['weeks'][$w]['topics'] as $topic ) { ?>
                                                                <li><?=$topic?></li>
                                                            <? } ?>
                                                        </ul>
                                                    <? } ?>
                                                <? } ?>
                                            </td>
                                            
                        <!-- In class -->   <td>
                                                <? foreach ( $courses as $key => $course ) { ?>
                                                    <? if ( !isset( $course['legacy-course'] ) ) { ?>
                                                        <p class='course<?=$key?>-background-light'><?= $course['course-code'] ?></p>
                                                    
                                                        <? if ( sizeof( $course['weeks'][$w]['lectures'] ) > 0 ) { ?> <p class="topic-header">Lectures:</p>
                                                            <ul>
                                                                <? foreach( $course['weeks'][$w]['lectures'] as $lecture ) { echo( ListItem( $lecture, "Lecture" ) ); } ?>
                                                            </ul>
                                                         <? } ?>
                                                         
                                                        <? if ( sizeof( $course['weeks'][$w]['exercises'] ) > 0 ) { ?> <p class="topic-header">Exercises:</p>
                                                            <ul>
                                                                <? foreach( $course['weeks'][$w]['exercises'] as $exercise ) { echo( ListItem( $exercise, "Exercise" ) ); } ?>
                                                            </ul>
                                                         <? } ?>
                                                         
                                                        <? if ( sizeof( $course['weeks'][$w]['projects'] ) > 0 ) { ?> <p class="topic-header">Projects:</p>
                                                            <ul>
                                                                <? foreach( $course['weeks'][$w]['projects'] as $project ) { echo( ListItem( $project, "Project" ) ); } ?>
                                                            </ul>
                                                         <? } ?>
                                                         
                                                        <? if ( sizeof( $course['weeks'][$w]['exams'] ) > 0 ) { ?> <p class="topic-header">Exam:</p>
                                                            <ul>
                                                                <? foreach( $course['weeks'][$w]['exams'] as $exam ) { echo( ListItem( $exam, "Exam" ) ); } ?>
                                                            </ul>
                                                         <? } ?>
                                                     <? } ?>
                                                 <? } ?>
                                            </td>
                                            
                        <!-- Homework -->   <td>
                                                <? foreach ( $courses as $key => $course ) { ?>
                                                    <? if ( !isset( $course['legacy-course'] ) ) { ?>
                                                        <p class='course<?=$key?>-background-light'><?= $course['course-code'] ?></p>
                                                        <? if ( sizeof( $course['weeks'][$w]['homework'] ) > 0 ) { ?> <p class="topic-header">Homework:</p>
                                                            <ul>
                                                                <? foreach( $course['weeks'][$w]['homework'] as $homework ) { echo( ListItem( $homework, "Notes" ) ); } ?>
                                                            </ul>
                                                         <? } ?>
                                                        <? if ( sizeof( $course['weeks'][$w]['quizzes'] ) > 0 ) { ?> <p class="topic-header">Quizzes:</p>
                                                            <ul>
                                                                <? foreach( $course['weeks'][$w]['quizzes'] as $quiz ) { echo( ListItem( $quiz, "Canvas" ) ); } ?>
                                                            </ul>
                                                         <? } ?>
                                                     <? } ?>
                                                 <? } ?>
                                            </td>
                        <!-- Notes -->      <td>
                                                <? foreach ( $courses as $key => $course ) { ?>
                                                    <? if ( !isset( $course['legacy-course'] ) ) { ?>
                                                        <p class='course<?=$key?>-background-light'><?= $course['course-code'] ?></p>
                                                        <? if ( sizeof( $course['weeks'][$w]['due'] ) > 0 ) { ?>
                                                            <? foreach( $course['weeks'][$w]['due'] as $due ) {
                                                                    echo( "<p class='topic-header due-date'> Due " . $due['day'] . ": </p>" );
                                                                    echo( "<ul class='due-date'>" );
                                                                    foreach( $due['due-items'] as $item ) {
                                                                        echo( "<li>" . $item . "</li>" );
                                                                    }
                                                                    echo( "</ul>" );
                                                                } ?>
                                                         <? } ?>

                                                         <? foreach( $course['weeks'][$w]['notes'] as $note ) {
                                                             echo( "<p>" . $note . "</p>" );
                                                         } ?>
                                                     <? } ?>
                                                 <? } ?>
                                            </td>
                    </tr>
                <? } ?>
            </table>
        </div>

    </div>
    
    

<? include_once( "lms-content/php/layouts/home-footer.php" ); ?>
