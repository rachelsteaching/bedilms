<? include_once( "lms-content/php/layouts/home-header.php" ); ?>

    <h1> Rachel's educational hub </h1>


    <a name="courses">&nbsp;</a><h2> Courses </h2>
    <p><a href="calendar.php">Or calendar of all classes</a></p>
    
    <div class="course-list row">

        <? $i = 1; foreach ( Storage::AllCourses() as $index => $course ) { ?>
            
            <div class="lms-course col-md-4 col-sm-2 col-xs-12">
                <div class="lms-course-inner course<?=$i?>-background-light">
                    <div class="lms-course-header course<?=$i?>-background course<?=$i?>-foreground"><h3><?= $course["course-code"]?>: <?= $course["course-name"]?><br><div class="school">(<?= $course["school"]?>)</div></h3></div>
                    <ul class="lms-course-links">
                        <? if ( isset( $course['legacy-course'] ) ) { ?>
                            <li> <a href="<?=$course['legacy-course'] ?>" class="course<?=$i?>-link"><img src="lms-content/graphics/icon-gotocourse.png"> Go to course</a> <sub>(old)</sub> </li>
                        <? } else { ?>
                            <li> <a href="course-home.php?course=<?=$course['course-code'] ?>" class="course<?=$i?>-link"><img src="lms-content/graphics/icon-gotocourse.png"> Go to course</a> </li>
                        <? } ?>
                        <li> <a href="<?= $course['course-repository'] ?>" class="course<?=$i?>-link"><img src="lms-content/graphics/icon-repository.png"> Repository</a> </li>
                        <li> <a href="<?= $course['course-catalog-url'] ?>" class="course<?=$i?>-link"><img src="lms-content/graphics/icon-description.png"> Course description</a> </li>
                        <li> <a href="<?= $course['canvas-url'] ?>" class="course<?=$i?>-link"><img src="lms-content/graphics/icon-canvas.png"> Canvas LMS</a> </li>
                    </ul>
                </div>
            </div>
        <? $i++; } ?>
        
    </div> <!-- course-list row -->
    
    <a name="about">&nbsp;</a><h2> About the instructor </h2>
    <div class="about row">
        <div class="col-md-2 instructor-image">
            <img src="page-config/instructor.png">
        </div>
        <div class="col-md-10">
            <p><?= $page['about'] ?></p>

            <ul class="social-media cf">
                <? foreach ( $page['emails'] as $email ) { ?>
                    <li> <a href="mailto:<?=$email?>"><img src="lms-content/graphics/social-email.png"    title="Send an email to <?=$email?>"> <?=$email?></a></li>
                <? } ?>
                <? foreach ( $page['social-media'] as $social ) { ?>
                    <li> <a href="<?=$social['url']?>"><img src="lms-content/graphics/social-<?=strtolower( $social['name'] )?>.png" title="View <?=$social['name']?> page"> <?=$social['name']?></a></li>
                <? } ?>
            </ul>
        </div>
    </div> <!-- about row -->
    
    <a name="resources">&nbsp;</a><h2> Additional resources </h2>
    <div class="resources pushme">
        <div class="col-md-12">
            <? foreach ( $page['additional-resources'] as $title => $resources ) { ?>
                <div class="pushme">
                    <h3><?=$title?></h3>

                    <? foreach( $resources as $resource ) { ?>
                        <div class="pushme">
                            <h4> <?=$resource['name'] ?> </h4>
                            <p> <?=$resource['description'] ?> </p>
                            <p> <a href="<?=$resource['url'] ?>"><?=$resource['url'] ?></a> </p>
                        </div>
                    <? } ?>
                </div>
            <? } ?>
        </div>
    </div> <!-- resources row -->

<? include_once( "lms-content/php/layouts/home-footer.php" ); ?>
