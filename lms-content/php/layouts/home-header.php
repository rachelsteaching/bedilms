<!DOCTYPE html>
<html lang="en">

<? include_once( "lms-content/php/backend.php" ); ?>
<? Storage::Init(); ?>
<? Storage::InitHomepage(); ?>
<? $page = Storage::Page(); ?>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="lms-content/graphics/favicon.png">

    <title> Homepage (<?= $page[ "instructor" ] ?>) //BediLMS </title>
    
    <link href="lms-content/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="lms-content/bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    
    <link href="lms-content/css/base.css" rel="stylesheet">
    
    <style type="text/css">
        .icon-bar { background-color: #fff; margin-top: 4px; display: block; width: 22px; height: 2px; border-radius: 1px;}
        .navbar-toggle { background: none; border: none; }
        .lms-navbar ul { list-style-type: none; margin-top: 20px; position: relative; }
        .lms-navbar ul li { float: right; .5rem 1rem }
    </style>
  </head>

  <body>
    <!-- PAGE BEGIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

    <nav class="navbar navbar-inverse navbar-fixed-top lms-navbar">

        <div class="navbar-header">
          <a class="navbar-brand" href="index.php"> <?= $page['instructor'] ?> </a> 
        </div>
        <div id="navbar">
          <!-- NAVIGATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
          <ul class="cf">
              <li class="nav-item"> <a href="#courses"   class="nav-link"> Courses </a> </li>
              <li class="nav-item"> <a href="#about"     class="nav-link"> About the instructor </a> </li>
              <li class="nav-item"> <a href="#resources" class="nav-link"> Additional resources </a> </li>
          </ul>
          <!-- NAVIGATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        </div><!--/.navbar-collapse -->
    </nav>


    <div class="container-fluid page-main">
      <div class="row">
          <div class="col-md-12">
