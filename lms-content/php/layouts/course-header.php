<!DOCTYPE html>
<html lang="en">

<? include_once( "lms-content/php/backend.php" ); ?>
<? Storage::Init(); ?>
<? Storage::InitCoursePage(); ?>
<?
$course = Storage::Course();
$page = Storage::Page();
$courseCode = $_GET["course"];
?>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="lms-content/graphics/favicon.png">

    <title> <?= $pageTitle ?> - <?=$course["course-code"] ?> (<?= $page[ "instructor" ] ?>) //BediLMS </title>
    
    <link href="lms-content/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="lms-content/bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="lms-content/bootstrap/jumbotron.css" rel="stylesheet">
    
    <link href="lms-content/css/base.css" rel="stylesheet">

    <style type="text/css">
        .icon-bar { background-color: #fff; margin-top: 4px; display: block; width: 22px; height: 2px; border-radius: 1px;}
        .navbar-toggle { background: none; border: none; }
        .lms-navbar ul { list-style-type: none; margin-top: 20px; position: relative; }
        .lms-navbar ul li { float: right; .5rem 1rem }
    </style>
  </head>

  <body>
    <!-- PAGE BEGIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

    <nav class="navbar navbar-inverse navbar-fixed-top lms-navbar">

        <div class="navbar-header">
          <a class="navbar-brand" href="course-home.php?course=<?=$courseCode?>"> <?= $course['course-code'] ?>: <?= $course['course-name'] ?> </a> 
        </div>
        <div id="navbar">
          <!-- NAVIGATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
          <ul class="cf">
              <li class="nav-item"> <a href="course-home.php?course=<?=$courseCode?>"   class="nav-link"> Course Home </a> </li>
              <li class="nav-item"> <a href="<?= $course['syllabus'] ?>"   class="nav-link"> Syllabus </a> </li>
              <li class="nav-item"> <a href="course-textbook.php?course=<?=$courseCode?>"   class="nav-link"> Textbook </a> </li>
              <li class="spacer">  </li>
              <li class="nav-item"> <a href="course-byweek.php?course=<?=$courseCode?>"     class="nav-link"> View by week </a> </li>
              <li class="nav-item"> <a href="course-byassignment.php?course=<?=$courseCode?>" class="nav-link"> View by assignment </a> </li>
              <li class="spacer">  </li>
              <li class="nav-item"> <a href="<?= $page[ "base-url" ] ?>" class="nav-link"><?= str_replace( "http://", "", $page[ "base-url" ] ) ?></a> </li>
          </ul>
          <!-- NAVIGATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        </div><!--/.navbar-collapse -->
    </nav>


    <div class="container-fluid page-main">
      <div class="row">
          <div class="col-md-12">
