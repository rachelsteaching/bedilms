<?
class Storage {
    private static $pagePath = "page-config/page.json";
    private static $coursePath = "course-content/";
    
    private static $pageInfo;
    private static $courseList = array();
    private static $courseInfo = array();
    
    private static function ReadJson( $path )
    {
        $raw = file_get_contents( $path );
        $array = json_decode( $raw, true );
        return $array;
    }

    private static function WriteJson( $path, $array )
    {
        $raw = json_encode( $array, JSON_PRETTY_PRINT );
        if ( $raw === FALSE ) { echo( "<p>Error: couldn't load " . $path . "!</p>" ); }

        if ( file_put_contents( $path, $raw ) )
        {
            // Success
        }
        else
        {
            // Fail
            DisplayError( "Failed to save grocery list!" );
        }
    }

    private static function GetListOfCourses()
    {
        return scandir( "course-content" );
    }

    private static function LoadCourses()
    {
        foreach ( self::GetListOfCourses() as $courseFile )
        {
            if ( strpos( $courseFile, ".json" ) !== false )
            {
                $courseData = self::ReadJson( "course-content/" . $courseFile  );
                array_push( self::$courseList, $courseData );
            }
        }
    }

    public static function Init()
    {
        self::$pageInfo = self::ReadJson( self::$pagePath );
    }

    public static function InitHomepage()
    {
        self::LoadCourses();
    }

    public static function InitCoursePage()
    {
        self::$courseInfo = self::ReadJson( self::$coursePath . strtolower( $_GET["course"] ) . ".json" );
    }

    public static function AllCourses()
    {
        return self::$courseList;
    }

    public static function Course()
    {
        return self::$courseInfo;
    }

    public static function Page()
    {
        return self::$pageInfo;
    }

    public static function GetTotalClassWeeks( $course = null )
    {
        if ( $course === null )
        {
            $course = self::$courseInfo;
        }
        
        $firstday = $course['first-day-first-week'];
        $lastday = $course['last-day-last-week'];
        $diff = strtotime($lastday, 0) - strtotime($firstday, 0);
        return floor( $diff / 604800 ) + 1;
    }

    public static function GetDayOfClass( $weekNumber, $dayOfWeek, $course = null )
    {
        if ( $course === null )
        {
            $course = self::$courseInfo;
        }
        
        $week = strtotime( $course['first-week'] . " +" . ($weekNumber-1) . " weeks next $dayOfWeek" );
        return $week;
    }

    public static function GetTodaysClassWeek( $course = null )
    {
        if ( $course === null )
        {
            $course = self::$courseInfo;
        }

        $totalWeeks = self::GetTotalClassWeeks( $course );

        $today = date( "Y-m-d" );
        for ( $w = 1; $w < $totalWeeks; $w++ )
        {
            $start = date( "Y-m-d", self::GetDayOfClass( $w, "Monday", $course ) );
            $end = date( "Y-m-d", self::GetDayOfClass( $w + 1, "Monday", $course ) );

            if ( $today >= $start && $today < $end )
            {
                return $w;
            }
        }
        return -1;
    }

    public static function DayMonthDate( $date )
    {
        return date( "D, M j", $date );
    }

    public static function MonthDate( $date )
    {
        return date( "M j", $date );
    }
};
?>
