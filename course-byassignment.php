<? $pageTitle = "View By Assignment"; ?>
<? include_once( "lms-content/php/layouts/course-header.php" ); ?>

<?
function ListItem( $item, $type, $icon )
{
    if ( $item['url'] == "" )
    {
        return "<img src='lms-content/graphics/" . $icon . "-icon.png' title='" . $type . "'> " . $item['name'];
    }
    else
    {
        return "<a href='" . $item['url'] . "'> <img src='lms-content/graphics/" . $icon . "-icon.png' title='" . $type . "'> " . $item['name'] . " </a>";
    }
}
?>

<?
function ListItems( $course, $name, $plural, $anchor, $icon )
{
    $lower = strtolower( $name );
    $lplural = strtolower( $plural );
    
    echo( "<!-- " . $plural . " -->" );
    echo( "<tr class='lms-anchor-link'><td colspan='3'><a name='" . $anchor . "'> <h4>" . $plural . "</h4> </a></td></tr> <!-- Link -->" );
    echo( "<tr> <th> Assignment Type </th> <th> Assignment </th> <th> Week </th> </tr>" );
    
    for ( $w = 1; $w <= Storage::GetTotalClassWeeks(); $w++ ) {
    
        foreach( $course['weeks'][$w][ $lplural ] as $item )
        {
            echo( "<tr> \n" .
            "<!-- Type -->       <td> " . $name . " </td> \n" .
            "<!-- Assignment --> <td> " . ListItem( $item, $name, $icon ) . " </td> \n" .
            "<!-- Week -->       <td> " . $w . " </td> \n </tr> " );
        }
    }
}
?>

    <div class="row">
        <div class="col-md-2">
            <h3>Assignment types</h3>

            <? $currentWeek = Storage::GetTodaysClassWeek(); ?>

            <ul class="list-of-weeks">
                <li><a href="#lectures">Lectures</a></li>
                <li><a href="#exercises">Exercises</a></li>
                <li><a href="#homework">Homework</a></li>
                <li><a href="#quizzes">Quizzes</a></li>
                <li><a href="#projects">Projects</a></li>
                <li><a href="#exams">Exams</a></li>
            </ul>
        </div>
        <div class="col-md-10">
            <a name="courses">&nbsp;</a><h2> Course by assignment </h2>
            <p> <?= $course['course-code'] ?>: <?= $course['course-name'] ?>, &nbsp; <?=$course['semester']?> </p>

            <table class="table week-by-week">
                
                
                <?
                ListItems( $course, "Lecture", "Lectures", "lectures", "lecture" );
                ListItems( $course, "Exercise", "Exercises", "exercises", "lecture" );
                ListItems( $course, "Homework", "Homework", "homework", "notes" );
                ListItems( $course, "Quiz", "Quizzes", "quizzes", "canvas" );
                ListItems( $course, "Project", "Projects", "projects", "project" );
                ListItems( $course, "Exam", "Exams", "exams", "exam" );
                ?>

                
            </table>
        </div>

    </div>
    
    

<? include_once( "lms-content/php/layouts/home-footer.php" ); ?>
